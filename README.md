# mongoStudy

#### 介绍

mongoHelper示例项目，演示了简单的增删改查功能，可以作为脚手架。

使用前请先在数据库admin表中添加一条数据，用于登录系统。

```
db.admin.insert([{name:"admin",pass:"admin"}])
``` 


#### mongoHelper项目介绍

spring-data-mongodb增强工具包，简化 CRUD 操作，提供类mybatis plus的数据库操作

https://gitee.com/cym1102/mongoHelper

