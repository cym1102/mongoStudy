package com.cym.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.annotation.Rollback;

import com.cym.MongoStudyClient;
import com.cym.model.User;

import cn.craccd.mongoHelper.bean.Page;
import cn.craccd.mongoHelper.bean.SortBuilder;
import cn.craccd.mongoHelper.utils.MongoHelper;

@SpringBootTest(classes = MongoStudyClient.class)
public class TestTool {

	@Autowired
	MongoHelper mongoHelper;

	@Test
	@Rollback(false)
	public void test() throws Exception {

		Page page = new Page();
		page.setLimit(100);
		SortBuilder sortBuilder = new SortBuilder(User::getName, Direction.ASC);

		page = mongoHelper.findPage(sortBuilder, page, User.class);
	}

}
