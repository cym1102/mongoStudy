package com.cym.config;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import cn.craccd.mongoHelper.utils.ImportExportUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;

@Configuration // 1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling // 2.开启定时任务
public class ScheduleTask {
	@Value("${spring.application.name}")
	String name;
	
	@Autowired
	ImportExportUtil importExportUtil;

	@Scheduled(cron = "0 0 0 * * ?")
	public void mysqlTasks() {
		String basePath = "";
		if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
			basePath = "d:";
		} else {
			basePath = "/home/bak/" + name;
		}

		String path = basePath + "/" + DateUtil.format(new Date(), "yyyy/MM/dd") + "/";
		FileUtil.mkdir(path);
		importExportUtil.exportDb(path);
	}
	

}