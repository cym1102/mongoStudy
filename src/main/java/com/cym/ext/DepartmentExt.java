package com.cym.ext;

import com.cym.model.Department;

import cn.craccd.mongoHelper.bean.IgnoreDocument;

@IgnoreDocument
public class DepartmentExt extends Department{
	
	Long userCount;

	public Long getUserCount() {
		return userCount;
	}

	public void setUserCount(Long userCount) {
		this.userCount = userCount;
	}

}
