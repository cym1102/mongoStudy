package com.cym.ext;

import com.cym.model.Department;
import com.cym.model.User;

import cn.craccd.mongoHelper.bean.IgnoreDocument;

@IgnoreDocument
public class UserExt extends User{
	
	Department department;

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}


}
