package com.cym;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.session.data.mongo.config.annotation.web.http.EnableMongoHttpSession;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@EnableMongoHttpSession
@SpringBootApplication
public class MongoStudyClient {

	public static void main(String[] args) {
		SpringApplication.run(MongoStudyClient.class, args);
	}
}
