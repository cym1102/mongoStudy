package com.cym.model;

import org.springframework.data.mongodb.core.mapping.Document;

import cn.craccd.mongoHelper.bean.BaseModel;
import cn.craccd.mongoHelper.bean.InitValue;

/**
 * 用户
 * 
 * @author 陈钇蒙
 *
 */
@Document
public class User extends BaseModel {
	String avatar;

	String name;
	String phone;

	String pass;
	
	String departmentId;
	
	

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}


	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
