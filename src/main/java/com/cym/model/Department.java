package com.cym.model;

import org.springframework.data.mongodb.core.mapping.Document;

import cn.craccd.mongoHelper.bean.BaseModel;

@Document
public class Department extends BaseModel{
	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
