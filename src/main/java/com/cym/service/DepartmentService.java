package com.cym.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cym.model.Department;
import com.cym.model.User;

import cn.craccd.mongoHelper.bean.Page;
import cn.craccd.mongoHelper.utils.CriteriaAndWrapper;
import cn.craccd.mongoHelper.utils.MongoHelper;
import cn.hutool.core.util.StrUtil;

@Service
public class DepartmentService {
	@Autowired
	MongoHelper mongoHelper;

	public Page<Department> search(Page<?> page, String word) {
		CriteriaAndWrapper criteriaAndWrapper = new CriteriaAndWrapper();

		if (StrUtil.isNotEmpty(word)) {
			criteriaAndWrapper.like(Department::getName, word);
		}

		Page<Department> pageResp = mongoHelper.findPage(criteriaAndWrapper, page, Department.class);

		return pageResp;
	}

	public Long getUserCount(String id) {
		return mongoHelper.findCountByQuery(new CriteriaAndWrapper().eq(User::getDepartmentId, id), User.class);
	}
}
