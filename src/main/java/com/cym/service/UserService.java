package com.cym.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cym.model.User;

import cn.craccd.mongoHelper.bean.Page;
import cn.craccd.mongoHelper.utils.CriteriaAndWrapper;
import cn.craccd.mongoHelper.utils.CriteriaOrWrapper;
import cn.craccd.mongoHelper.utils.MongoHelper;
import cn.hutool.core.util.StrUtil;

@Service
public class UserService {
	@Autowired
	MongoHelper mongoHelper;

	public Page<User> search(Page<?> page, String word) {
		CriteriaAndWrapper criteriaAndWrapper = new CriteriaAndWrapper();

		if (StrUtil.isNotEmpty(word)) {
			criteriaAndWrapper.and(new CriteriaOrWrapper().like(User::getName, word).like(User::getPhone, word));
		}
		
		Page<User> pageResp = mongoHelper.findPage(criteriaAndWrapper, page, User.class);

		return pageResp;
	}

	public User login(String phone, String pass) {
		CriteriaAndWrapper criteriaAndWrapper = new CriteriaAndWrapper().eq(User::getPhone, phone).eq(User::getPass, pass);
		
		return mongoHelper.findOneByQuery(criteriaAndWrapper, User.class);
	}




}
