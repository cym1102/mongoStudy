package com.cym.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cym.model.Admin;

import cn.craccd.mongoHelper.bean.Page;
import cn.craccd.mongoHelper.utils.CriteriaAndWrapper;
import cn.craccd.mongoHelper.utils.MongoHelper;
import cn.hutool.core.util.StrUtil;

@Service
public class AdminService  {
	@Autowired
	MongoHelper mongoHelper;
	
	public Page<Admin> search(Page<?> page, String word) {
		CriteriaAndWrapper criteriaAndWrapper = new CriteriaAndWrapper();

		if (StrUtil.isNotEmpty(word)) {
			criteriaAndWrapper.like(Admin::getName, word);
		}
		Page<Admin> pageResp = mongoHelper.findPage(criteriaAndWrapper, page, Admin.class);

		return pageResp;
	}

	public Admin login(String name, String pass) {
		CriteriaAndWrapper criteriaAndWrapper = new CriteriaAndWrapper().eq(Admin::getName, name).eq(Admin::getPass, pass);
		
		return mongoHelper.findOneByQuery(criteriaAndWrapper, Admin.class);
	}

	public void add(Admin admin) {
		mongoHelper.insert(admin);
		
	}

}
