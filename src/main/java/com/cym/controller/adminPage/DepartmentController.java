package com.cym.controller.adminPage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cym.ext.DepartmentExt;
import com.cym.model.Department;
import com.cym.service.DepartmentService;
import com.cym.utils.BaseController;
import com.cym.utils.JsonResult;

import cn.craccd.mongoHelper.bean.Page;
import cn.craccd.mongoHelper.utils.BeanExtUtil;

@Controller
@RequestMapping("/adminPage/department")
public class DepartmentController extends BaseController {
	@Autowired
	DepartmentService departmentService;

	@RequestMapping("")
	public ModelAndView index(ModelAndView modelAndView, Page<?> page, String keywords) {

		page = departmentService.search(page, keywords);
		Page<DepartmentExt> pageExt = BeanExtUtil.copyPageByProperties(page, DepartmentExt.class); 
		
		for (DepartmentExt departmentExt : pageExt.getList()) {
			departmentExt.setUserCount(departmentService.getUserCount(departmentExt.getId()));
		}
		
		modelAndView.addObject("keywords", keywords);
		modelAndView.addObject("page", pageExt);
		modelAndView.setViewName("/adminPage/department/index");
		return modelAndView;
	}

	@RequestMapping("addOver")
	@ResponseBody
	public JsonResult addOver(Department department) {
		mongoHelper.insertOrUpdate(department);

		return renderSuccess();
	}

	@RequestMapping("detail")
	@ResponseBody
	public JsonResult detail(String id) {
		Department department = mongoHelper.findById(id, Department.class);
		
		return renderSuccess(department);
	}

	@RequestMapping("del")
	@ResponseBody
	public JsonResult del(String id) {
		Long count = departmentService.getUserCount(id);
		if(count > 0) {
			return renderError("此部门已有员工,不可删除");
		}
		
		mongoHelper.deleteById(id, Department.class);
		return renderSuccess();
	}
	
	
	
}
